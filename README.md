# M347 Docker-Aufgabe 03.02 Intermediate

## Dokumentation

**1. Verwenden Sie ein beliebiges Projekt oder wahlweise die Projektvorlage Simple Dockerfile zum erstellen von Container Images.**

Ich habe die Simple Dockerfile zum erstellen von Container Images verwendet.

**2. Erstellen Sie mehrere Images, welche sich inhaltlich zumindest minimal unterscheiden, beispielsweise durch die unterschiedliche Ausgabe auf der Konsole. Beachten Sie dazu auch den entsprechenden Kommentar im Dockerfile.**

Diese Befehle habe ich verwendet um einen Image zu erstellen:

1. Image:
- `docker build -t simpledocker1 --build-arg Version 1.0 --build-arg Username=dgozutok C:\users\goezuetd\`
2. Image:
- `docker build -t simpledocker2 --build-arg Version 1.0 --build-arg Username=korm C:\users\goezuetd\`

**3. Verwenden Sie die Container Registry dieses Projekts um die zuvor erstellten Varianten des Images zu publizieren.**

Um die erstellten Image auf meinem Container Registry zu publizieren, musste ich diese Befehle ausführen.

1. Image:
- `docker login registry.gitlab.com`
- `docker tag simpledocker1:latest registry.gitlab.com/dgozutok/m347_3.2/simpledocker1:latest`
- `docker push registry.gitlab.com/dgozutok/m347_3.2/simpledocker1:latest`

2. Image:
- `docker login registry.gitlab.com`
- `docker tag simpledocker2:latest registry.gitlab.com/dgozutok/m347_3.2/simpledocker2:latest`
- `docker push registry.gitlab.com/dgozutok/m347_3.2/simpledocker2:latest`


**4. Publizieren Sie zudem die Version 2.0 des Images m347/simple-dockerfile mit dem Tag from-template in der Container Registry ihres eigenen Projekts auf GitLab.**

Ich habe hier einen neuen Image mit dem Version 2.0 erstellt und mit dem Tag :from-template publiziert.
Hier sind die Schritte:

- `docker build -t simple-dockerfile:from-template --build-arg Version 2.0 --build-arg Username=forstm C:\users\goezuetd\`
- `docker tag simple-dockerfile:from-template registry.gitlab.com/dgozutok/m347_3.2/simple-dockerfile:from-template`
- `docker push registry.gitlab.com/dgozutok/m347_3.2/simple-dockerfile:from-template`
